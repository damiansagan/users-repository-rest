package com.damiansagan.usersrepositoryrest.user;

import com.damiansagan.usersrepositoryrest.UsersRepositoryRestApplication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = UsersRepositoryRestApplication.USERS_PATH)
interface UserRepository extends CrudRepository<User, Long> {
}
