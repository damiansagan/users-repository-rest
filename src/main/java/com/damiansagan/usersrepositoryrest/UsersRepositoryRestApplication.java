package com.damiansagan.usersrepositoryrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.handler.MappedInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class UsersRepositoryRestApplication {

    public static final String USERS_PATH = "users";

    @Bean
    public MappedInterceptor cacheControlInterceptor() {
        return new MappedInterceptor(new String[]{"/**/" + USERS_PATH + "/**/"}, new HandlerInterceptor() {
            @Override
            public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
                CacheControl cacheControl = CacheControl.maxAge(15, TimeUnit.SECONDS).cachePrivate().mustRevalidate();
                response.setHeader("Cache-Control", cacheControl.getHeaderValue());
                return true;
            }
        });
    }

    public static void main(String[] args) {
        SpringApplication.run(UsersRepositoryRestApplication.class, args);
    }
}
